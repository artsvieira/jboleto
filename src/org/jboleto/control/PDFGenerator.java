/*
 * Esta biblioteca e um software livre, que pode ser redistribuido e/ou 
 * modificado sob os termos da Licença Publica Geral Reduzida GNU, 
 * conforme publicada pela Free Software Foundation, versao 2.1 da licenca.
 *
 * Esta biblioteca e distribuida na experanca de ser util aos seus usuarios, 
 * porem NAO TEM NENHUMA GARANTIA, EXPLICITAS OU IMPLICITAS, COMERCIAIS OU 
 * DE ATENDIMENTO A UMA DETERMINADA FINALIDADE. 
 * Veja a Licenca Publica Geral Reduzida GNU para maiores detalhes. 
 * A licenca se encontra no arquivo lgpl-br.txt 
 */

package org.jboleto.control;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;

import javax.swing.text.DefaultFormatter;
import javax.swing.text.NumberFormatter;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.BarcodeInter25;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.util.Vector;
import org.jboleto.Banco;
import org.jboleto.JBoletoBean;

/**
 *
 * Classe responsavel pela geracao dos boletos no formato PDF.
 * @author Fabio Souza
 */
public class PDFGenerator {
    
    ByteArrayOutputStream baos;
    
    private DefaultFormatter formatter;
	
	private Document document;
	private PdfContentByte cb;
    
	private PdfTemplate tempImgBoleto;
    private PdfTemplate tempImgBanco;
    private PdfTemplate tempImgBarcode;

	private float IMAGEM_BOLETO_WIDTH = 514.22f;
	//private float IMAGEM_BOLETO_HEIGHT = 385.109f;
	private float IMAGEM_BOLETO_HEIGHT = 690.109f; 
    
	private float IMAGEM_BANCO_WIDTH = 100.0f;
	private float IMAGEM_BANCO_HEIGHT = 23.0f; 
	
	private float IMAGEM_BARCODE_WIDTH = 293.78f;
	private float IMAGEM_BARCODE_HEIGHT = 37.0f;
    
    Image imgTitulo = null;
    
    public PDFGenerator() {
        
        baos = new ByteArrayOutputStream();
        
		formatter = new NumberFormatter(new DecimalFormat("#,##0.00"));
		
		document = new Document(PageSize.A4);
		
		try {
            PdfWriter writer = PdfWriter.getInstance(document, baos);

            document.open();

            cb = writer.getDirectContent();
            
            //gera template com o fundo do boleto
            //imgTitulo = Image.getInstance(getClass().getResource("/img/template.png"));
            imgTitulo = Image.getInstance(getClass().getResource("/img/template2.png"));
            imgTitulo.scaleToFit(IMAGEM_BOLETO_WIDTH,IMAGEM_BOLETO_HEIGHT);
            imgTitulo.setAbsolutePosition(0,0);
            
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}          
        
    }

	/**
	 * Adiciona um boleto na fila.
	 */
	@SuppressWarnings("unchecked")
	public void addBoleto(JBoletoBean boleto, Banco banco) {
		try {
                        
            tempImgBoleto = cb.createTemplate(IMAGEM_BOLETO_WIDTH,IMAGEM_BOLETO_HEIGHT);                
            tempImgBoleto.addImage(imgTitulo);       

            //gera template com a imagem do logo do banco
            Image imgBanco = Image.getInstance(getClass().getResource("/img/" + banco.getNumero() + ".png"));
            imgBanco.scaleToFit(IMAGEM_BANCO_WIDTH,IMAGEM_BANCO_HEIGHT);
            imgBanco.setAbsolutePosition(0,0);
            
            tempImgBanco = cb.createTemplate(IMAGEM_BANCO_WIDTH,IMAGEM_BANCO_HEIGHT);               
            tempImgBanco.addImage(imgBanco);            
            		
			float altura = 412;
			
			document.newPage();
			
	        cb.addTemplate(tempImgBoleto, document.left(),document.top()-750);	        
            cb.addTemplate(tempImgBanco, document.left(),document.top()-486);
            cb.addTemplate(tempImgBanco, document.left(),document.top()-179);
            //cb.addTemplate(tempImgBanco, document.left(),document.top()-135);
	        
            BaseFont bfTextoSimples = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
            BaseFont bfTextoCB = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.WINANSI, BaseFont.EMBEDDED);
            
            /** INICIO CEDENTE **/
            
            //inicio das descricoes do boleto
	 
	        cb.beginText();        	
	        cb.setFontAndSize(bfTextoCB,10);

            Vector descricoes = boleto.getDescricoes();
            
            if (descricoes != null){
            	for (int i=0; i < descricoes.size(); i++) {
            		cb.setTextMatrix(document.left(),document.top()-50+i*15);	        
            		cb.showText(String.valueOf(descricoes.elementAt(i)));                
            	}
            }
            
            cb.endText();
            
            //fim descricoes
 
	        cb.beginText();        	
	        cb.setFontAndSize(bfTextoSimples,8);

            cb.setTextMatrix(document.left()+50, altura+328);
            cb.showText(boleto.getCedente()); //imprime o cedente            
                        
            cb.setTextMatrix(document.left()+5,altura+308);
            String sacado = boleto.getNomeSacado();
            if (sacado.length() > 46)
            	sacado = sacado.substring(0, 46);
	        cb.showText(sacado);

        	cb.setTextMatrix(document.left()+230,altura+308);	        
	        cb.showText(boleto.getDataVencimento());
	        				
        	cb.setTextMatrix(document.left()+400,altura+308);	        
	        cb.showText(formatter.valueToString(new Double(boleto.getValorBoleto())));

        	cb.setTextMatrix(document.left()+5,altura+288);
        	String ContaCorrente   = boleto.getAgencia() + " / " + boleto.getContaCorrente();
        	String dvContaCorrente = boleto.getDvContaCorrente();
        	if (dvContaCorrente != null && !dvContaCorrente.equals(""))
        		ContaCorrente += "-" + dvContaCorrente;
	        cb.showText(ContaCorrente);
	        
        	cb.setTextMatrix(document.left()+146,altura+288);
        	String nossoNumero   = boleto.getNossoNumero();
        	String dvNossoNumero = banco.getDvNossoNumero(boleto.getNossoNumero());
        	if (dvNossoNumero != null && !dvNossoNumero.equals(""))
        		nossoNumero += "-" + dvNossoNumero;
	        cb.showText(nossoNumero);
	        cb.endText();
	        
	        /** FIM CEDENTE **/
	        
	        /** INICIO RECIBO SACADO **/
	        
	        cb.beginText();        	
	        cb.setFontAndSize(bfTextoCB,13);
	        
        	cb.setTextMatrix(document.left()+125,altura+220);	        
	        cb.showText(banco.getNumero() + "-" + banco.getDvNumero());
	        cb.endText();
            
	        cb.beginText();        	
	        cb.setFontAndSize(bfTextoSimples,8);
	        
	        if (boleto.isContraApresentacao()){
	        	cb.setTextMatrix(document.left()+5,altura+197);
	        	cb.showText(JBoletoBean.localContraApresentacao);
	        	
	        	cb.setTextMatrix(document.left()+395,altura+187);	        
		        cb.showText(JBoletoBean.msgContraApresentacao);
	        }
	        else{
	        
	        	cb.setTextMatrix(document.left()+5,altura+197);	        
		        cb.showText(boleto.getLocalPagamento());
	
	        	cb.setTextMatrix(document.left()+5,altura+187);	        
		        cb.showText(boleto.getLocalPagamento2());
		        
	        	cb.setTextMatrix(document.left()+425,altura+187);	        
		        cb.showText(boleto.getDataVencimento());
	        }

        	cb.setTextMatrix(document.left()+5,altura+167);	        
	        cb.showText(boleto.getCedente());

        	cb.setTextMatrix(document.left()+420,altura+167);
	        cb.showText(ContaCorrente);

        	cb.setTextMatrix(document.left()+5,altura+146);	        
	        cb.showText(boleto.getDataDocumento());
            
            cb.setTextMatrix(document.left()+70,altura+146);
            cb.showText((!(boleto.getNoDocumento().equalsIgnoreCase("")))? boleto.getNoDocumento() : boleto.getNossoNumero());  
            
        	cb.setTextMatrix(document.left()+180,altura+146);	        
	        cb.showText(boleto.getEspecieDocumento());

        	cb.setTextMatrix(document.left()+250,altura+146);	        
	        cb.showText(boleto.getAceite());

        	cb.setTextMatrix(document.left()+300,altura+146);	        
	        cb.showText(boleto.getDataProcessamento());

        	cb.setTextMatrix(document.left()+410,altura+146);
	        cb.showText(nossoNumero);
	        
	        cb.setTextMatrix(document.left()+5, altura+123);
	        cb.showText(boleto.getUsoBanco());
	        
        	cb.setTextMatrix(document.left()+122,altura+123);
        	String carteira = boleto.getCarteira();
        	if (boleto.getDvCarteira() != null && !boleto.getDvCarteira().equals("")){
        		carteira = carteira + "-" + boleto.getDvCarteira();
        	}
	        cb.showText(carteira);
	        
        	cb.setTextMatrix(document.left()+180,altura+123);	        
	        cb.showText("R$");
	        
        	cb.setTextMatrix(document.left()+430,altura+123);	        
        	cb.showText(formatter.valueToString(new Double(boleto.getValorBoleto())));
	        
        	cb.setTextMatrix(document.left()+5,altura+101);
        	String Instrucao1 = boleto.getInstrucao1();
        	if (Instrucao1.length() > 70){
        		Instrucao1 = Instrucao1.substring(0, 70);
        	}
	        cb.showText(Instrucao1);

        	cb.setTextMatrix(document.left()+5,altura+91);	        
        	String Instrucao2 = boleto.getInstrucao2();
        	if (Instrucao2.length() > 70){
        		Instrucao2 = Instrucao2.substring(0, 70);
        	}
	        cb.showText(Instrucao2);

        	cb.setTextMatrix(document.left()+5,altura+81);	        
        	String Instrucao3 = boleto.getInstrucao3();
        	if (Instrucao3.length() > 70){
        		Instrucao3 = Instrucao3.substring(0, 70);
        	}
	        cb.showText(Instrucao3);
	        
        	cb.setTextMatrix(document.left()+5,altura+71);	        
        	String Instrucao4 = boleto.getInstrucao4();
        	if (Instrucao4.length() > 70){
        		Instrucao4 = Instrucao4.substring(0, 70);
        	}
	        cb.showText(Instrucao4);

        	cb.setTextMatrix(document.left()+5,altura+61);	        
        	String Instrucao5 = boleto.getInstrucao5();
        	if (Instrucao5.length() > 70){
        		Instrucao5 = Instrucao5.substring(0, 70);
        	}
	        cb.showText(Instrucao5);

        	cb.setTextMatrix(document.left()+5,altura+31);	        
	        cb.showText(boleto.getCedente());
	        
        	cb.setTextMatrix(document.left()+100,altura+6);	        
	        cb.showText(boleto.getNomeSacado() + "     " + boleto.getCpfSacado());

        	cb.setTextMatrix(document.left()+100,altura-4);	        
	        cb.showText(boleto.getEnderecoSacado());
	        
        	cb.setTextMatrix(document.left()+100,altura-14);	        
	        cb.showText(boleto.getCepSacado() + " " + boleto.getBairroSacado() + " - " + boleto.getCidadeSacado() + " " + boleto.getUfSacado());
	        
	        cb.endText();
	        
	        /** FIM RECIBO SACADO **/
	        
	        
	        /** INICIO FICHA COMPENSACAO **/
	        
	        cb.beginText();        	
	        cb.setFontAndSize(bfTextoCB,13);

            cb.setTextMatrix(document.left()+125,altura-87);	        
                        
	        cb.showText(banco.getNumero() + "-" + banco.getDvNumero());
            cb.endText();
	        
	        cb.beginText();        	
	        cb.setFontAndSize(bfTextoCB,10);
	        
        	cb.setTextMatrix(document.left()+175,altura-87);	        
	        cb.showText(banco.getLinhaDigitavel());
	        cb.endText();
            
	        cb.beginText();        	
	        cb.setFontAndSize(bfTextoSimples,8);
	        
	        if (boleto.isContraApresentacao()){
	        	cb.setTextMatrix(document.left()+5,altura-111);
	        	cb.showText(JBoletoBean.localContraApresentacao);
	        	
	        	cb.setTextMatrix(document.left()+395,altura-121);	        
		        cb.showText(JBoletoBean.msgContraApresentacao);
	        }
	        else{
	        
	        	cb.setTextMatrix(document.left()+5,altura-111);	        
		        cb.showText(boleto.getLocalPagamento());

	        	cb.setTextMatrix(document.left()+5,altura-121);	        
		        cb.showText(boleto.getLocalPagamento2());

	        	cb.setTextMatrix(document.left()+425,altura-121);	        
		        cb.showText(boleto.getDataVencimento());
	        }
	        
        	cb.setTextMatrix(document.left()+5,altura-141);	        
	        cb.showText(boleto.getCedente());

        	cb.setTextMatrix(document.left()+420,altura-141);
	        cb.showText(ContaCorrente);

        	cb.setTextMatrix(document.left()+5,altura-162);	        
	        cb.showText(boleto.getDataDocumento());
            
            cb.setTextMatrix(document.left()+70,altura-162);
            cb.showText((!(boleto.getNoDocumento().equalsIgnoreCase("")))? boleto.getNoDocumento() : boleto.getNossoNumero());  
            
        	cb.setTextMatrix(document.left()+180,altura-162);	        
	        cb.showText(boleto.getEspecieDocumento());

        	cb.setTextMatrix(document.left()+250,altura-162);	        
	        cb.showText(boleto.getAceite());

        	cb.setTextMatrix(document.left()+300,altura-162);	        
	        cb.showText(boleto.getDataProcessamento());

        	cb.setTextMatrix(document.left()+410,altura-162);
	        cb.showText(nossoNumero);
	        
	        cb.setTextMatrix(document.left()+5, altura-185);
	        cb.showText(boleto.getUsoBanco());
	        
        	cb.setTextMatrix(document.left()+122,altura-185);	        
	        cb.showText(carteira);
	        
        	cb.setTextMatrix(document.left()+180,altura-185);	        
	        cb.showText("R$");
	        
        	cb.setTextMatrix(document.left()+430,altura-185);	        
        	cb.showText(formatter.valueToString(new Double(boleto.getValorBoleto())));
	        
        	cb.setTextMatrix(document.left()+5,altura-207);	        
	        cb.showText(boleto.getInstrucao1());

        	cb.setTextMatrix(document.left()+5,altura-217);	        
	        cb.showText(boleto.getInstrucao2());

        	cb.setTextMatrix(document.left()+5,altura-227);	        
	        cb.showText(boleto.getInstrucao3());
	        
        	cb.setTextMatrix(document.left()+5,altura-237);	        
	        cb.showText(boleto.getInstrucao4());

        	cb.setTextMatrix(document.left()+5,altura-247);	        
	        cb.showText(boleto.getInstrucao5());

        	cb.setTextMatrix(document.left()+5,altura-277);	        
	        cb.showText(boleto.getCedente());
	        
        	cb.setTextMatrix(document.left()+100,altura-302);	        
	        cb.showText(boleto.getNomeSacado() + "     " + boleto.getCpfSacado());

        	cb.setTextMatrix(document.left()+100,altura-312);	        
	        cb.showText(boleto.getEnderecoSacado());
	        
        	cb.setTextMatrix(document.left()+100,altura-322);	        
	        cb.showText(boleto.getCepSacado() + " " + boleto.getBairroSacado() + " - " + boleto.getCidadeSacado() + " " + boleto.getUfSacado());
	        
	        cb.endText();
	        
	        /** FIM FICHA COMPENSACAO **/
	        
	        BarcodeInter25 code = new BarcodeInter25();
	        code.setCode(banco.getCodigoBarras());
	        code.setExtended(true);
	        
	        code.setTextAlignment(Element.ALIGN_LEFT);
	        code.setBarHeight(37.00f);
	        code.setFont(null);	        
	        code.setX(0.73f);
	        code.setN(3);
	        
	        java.awt.Image barcode = code.createAwtImage(new Color(0, 0, 0), new Color(255, 255, 255));
	        //BMPFile convert = new BMPFile();
			//convert.saveBitmap("barcode.bmp", barcode, barcode.getWidth(null), barcode.getHeight(null));
	        
	        //Image imgBarcode = Image.getInstance("barcode.bmp");
	        
	        Image imgBarcode = Image.getInstance(barcode, null);
	        
	        //PdfTemplate imgCB = code.createTemplateWithBarcode(cb,null,null);
	        imgBarcode.setAbsolutePosition(0,0);
	        imgBarcode.scaleAbsoluteWidth(IMAGEM_BARCODE_WIDTH);
	        imgBarcode.scaleAbsoluteHeight(IMAGEM_BARCODE_HEIGHT);
            tempImgBarcode = cb.createTemplate(IMAGEM_BARCODE_WIDTH,IMAGEM_BARCODE_HEIGHT);               
            tempImgBarcode.addImage(imgBarcode); 
	        cb.addTemplate(tempImgBarcode, 40, 10);
	        //cb.addTemplate(imgCB,40,10);
	        
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}				
	}
	
    
    public ByteArrayOutputStream getBaos() {
        document.close();
        return baos;
    }
          
}
