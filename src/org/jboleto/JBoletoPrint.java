package org.jboleto;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PrintQuality;

import org.apache.pdfbox.pdmodel.PDDocument;



public class JBoletoPrint { 
	
	JBoletoPrint(){}

	public static void print (String FileName, String printerName) throws IOException, PrinterException{
			
			PDDocument document = PDDocument.load(FileName);
			
			HashPrintRequestAttributeSet prats = new HashPrintRequestAttributeSet();
			prats.add(MediaSizeName.ISO_A4);
			prats.add(OrientationRequested.PORTRAIT);
			prats.add(PrintQuality.HIGH);

			PrintService service = PrintServiceLookup.lookupDefaultPrintService(); 
			
			PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null); 
			for (int i = 0; i < services.length; i++)
			{
				String serviceName = services[i].getName();
				if (printerName.equals(serviceName))
				{
					service = services[i];
				}
			}
			
	        PrinterJob printJob = PrinterJob.getPrinterJob();
	        printJob.setPrintService(service);
	        printJob.setJobName(FileName);
	        printJob.setPageable(document);
	        printJob.print(prats);
	        //printJob.print();
	        document.close(); 

	}
} 