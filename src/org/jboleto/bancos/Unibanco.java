/*
 * Esta biblioteca e um software livre, que pode ser redistribuido e/ou 
 * modificado sob os termos da Licença Publica Geral Reduzida GNU, 
 * conforme publicada pela Free Software Foundation, versao 2.1 da licenca.
 *
 * Esta biblioteca e distribuida na experanca de ser util aos seus usuarios, 
 * porem NAO TEM NENHUMA GARANTIA, EXPLICITAS OU IMPLICITAS, COMERCIAIS OU 
 * DE ATENDIMENTO A UMA DETERMINADA FINALIDADE. 
 * Veja a Licenca Publica Geral Reduzida GNU para maiores detalhes. 
 * A licenca se encontra no arquivo lgpl-br.txt 
 */


package org.jboleto.bancos;

import org.jboleto.Banco;
import org.jboleto.JBoletoBean;

/**
 * Classe para gerar o boleto do unibanco
 * @author Fabio Souza
 */
public class Unibanco implements Banco {
	
	JBoletoBean boleto;
		
    /**
     * Metdodo responsavel por resgatar o numero do banco, coloque no return o codigo do seu banco
     */
	public String getNumero() {
        return "409";
    }
	
	public String getDvNumero() {
		return getDvNossoNumero(boleto.getNossoNumero());
	}
	
	public String getDvNossoNumero(String nossoNumero) {
		return getModulo11(nossoNumero,9);
	}
    
    /**
     * Método paticular do unibanco
     */
    public String getCvt() {
        return "5";
    }

    /**
     * Método paticular do unibanco
     */    
    public String getZero() {
        return "00";
    }
    
    /**
     * Metodo para gerar o digito verificador do nosso numero.
     * o parametro deve conter somente numeros
     */
    public String getDvNossoNumero() {
        return getModulo11(boleto.getNossoNumero(),9);
    }
    
    public String getNossoNumeroComDv() {
        return boleto.getNossoNumero() + getDvNossoNumero();
    }
    
    /**
     * Classe construtura, recebe como parametro a classe jboletobean
     */
	public Unibanco(JBoletoBean boleto) {
		this.boleto = boleto;		
	}
	
    /**
     * Metodo que monta o primeiro campo do codigo de barras 
     * Este campo como os demais e feito a partir do da documentacao do banco
     * A documentacao do banco tem a estrutura de como monta cada campo, depois disso
     * é só concatenar os campos como no exemplo abaixo.
     */
	private String getCampo1() {
		String campo = getNumero() + String.valueOf(boleto.getMoeda()) + getCvt() + boleto.getCodCliente().substring(0,4);

		return campo + boleto.getModulo10(campo);		
	}
	
    /**
     * ver documentacao do banco para saber qual a ordem deste campo 
     */
	private String getCampo2() {
		String campo = boleto.getCodCliente().substring(4) + getZero() + getNossoNumeroComDv().substring(0,5);		
		
		return campo + boleto.getModulo10(campo);
	}

    /**
     * ver documentacao do banco para saber qual a ordem deste campo
     */
	private String getCampo3() {
		String campo = getNossoNumeroComDv().substring(5);		
		
		return campo + boleto.getModulo10(campo);		
	}
	
    /**     
     * ver documentacao do banco para saber qual a ordem deste campo     
     */
	private String getCampo4() {
		String campo = 	getNumero() + String.valueOf(boleto.getMoeda()) + 
						boleto.getFatorVencimento() + boleto.getValorTitulo() + getCvt() + boleto.getCodCliente() + 
                        getZero() + String.valueOf(boleto.getNossoNumero()) + getDvNossoNumero();
								
		return boleto.getModulo11(campo,9);
	}
	
    /**
     * ver documentacao do banco para saber qual a ordem deste campo
     */
	private String getCampo5() {
		String campo = boleto.getFatorVencimento() + boleto.getValorTitulo();
		return campo;
	}
	
    /**
     * Metodo para monta o desenho do codigo de barras
     * A ordem destes campos tambem variam de banco para banco, entao e so olhar na documentacao do seu banco
     * qual a ordem correta
     */
	public String getCodigoBarras() {
		return getNumero() + String.valueOf(boleto.getMoeda()) + String.valueOf(getCampo4()) + String.valueOf(getCampo5()) + getCvt() + boleto.getCodCliente() + getZero() + boleto.getNossoNumero() + getDvNossoNumero();
	}

    /**
     * Metodo que concatena os campo para formar a linha digitavel
     * E necessario tambem olhar a documentacao do banco para saber a ordem correta a linha digitavel
     */    
    
	public String getLinhaDigitavel() {
		return 	getCampo1().substring(0,5) + "." + getCampo1().substring(5) + "  " + 
				getCampo2().substring(0,5) + "." + getCampo2().substring(5) + "  " +
				getCampo3().substring(0,5) + "." + getCampo3().substring(5) + "  " +
				getCampo4() + "  " + getCampo5(); 
	}

	public String getCarteira() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**************************************************************************
	 * 	getModulo11
	 *  @param String value
	 *  @param int type
	 * 	@return int dac
	 *  @author Mario Grigioni
	 */
    public String getModulo11(String campo,int type) {
    	//Modulo 11 - 234567   (type = 7)
    	//Modulo 11 - 23456789 (type = 9)
        
    	int multiplicador = 2;
		int multiplicacao = 0;
		int soma_campo = 0;
		
		for (int i = campo.length(); i > 0; i--) {
			multiplicacao = Integer.parseInt(campo.substring(i-1,i)) * multiplicador;
			
			soma_campo = soma_campo + multiplicacao;
			
			multiplicador++;
			if (multiplicador > 7 && type == 7)
				multiplicador = 2;
			else if (multiplicador > 9 && type == 9)
				multiplicador = 2;
		}
		
		int dac = 11 - (soma_campo%11);
		
        if (dac > 9 && type == 7)
            dac = 0;
        else if (dac > 9 && type == 9)
        	dac = 0;

        return ((Integer)dac).toString();
    }
	
}