/*
 * Esta biblioteca e um software livre, que pode ser redistribuido e/ou 
 * modificado sob os termos da Licença Publica Geral Reduzida GNU, 
 * conforme publicada pela Free Software Foundation, versao 2.1 da licenca.
 *
 * Esta biblioteca e distribuida na experanca de ser util aos seus usuarios, 
 * porem NAO TEM NENHUMA GARANTIA, EXPLICITAS OU IMPLICITAS, COMERCIAIS OU 
 * DE ATENDIMENTO A UMA DETERMINADA FINALIDADE. 
 * Veja a Licenca Publica Geral Reduzida GNU para maiores detalhes. 
 * A licenca se encontra no arquivo lgpl-br.txt 
 */


package org.jboleto.bancos;

import org.jboleto.Banco;
import org.jboleto.JBoletoBean;

/**
 * Classe responsavel em criar os campos do Banco do Brasil
 * @author Fabio Souza
 */
public class BancoBrasil implements Banco {
	
	JBoletoBean boleto;
		
	public String getNumero() {
        return "001";
    } 
	
	public String getDvNumero() {
		return "9";
	}

	public String getDvNossoNumero(String nossoNumero) {
		// TODO Auto-generated method stub
		return "";
	}
	    
	public BancoBrasil(JBoletoBean boleto) {
		this.boleto = boleto;		
        
        //System.out.println(getCampoLivre());
	}
    
    private String getCampoLivre() {
        String campo = "000000" + boleto.getNossoNumero() + boleto.getCarteira();
        
        return campo;
    }
    
	private String getCampo1() {
		String campo = getNumero() + boleto.getMoeda() + getCampoLivre().substring(0,5);

		return campo + boleto.getModulo10(campo);
	}
	
	private String getCampo2() {
		String campo = getCampoLivre().substring(5,15);// + boleto.getAgencia();
		
		return campo + boleto.getModulo10(campo);
	}

	private String getCampo3() {
		String campo = getCampoLivre().substring(15);
        
		return campo + boleto.getModulo10(campo);		
	}
	
	private String getCampo4() {
   		String campo = 	getNumero() + String.valueOf(boleto.getMoeda()) + 
        				boleto.getFatorVencimento() + boleto.getValorTitulo() + getCampoLivre();		
        
		return boleto.getModulo11(campo,9);	
	}
	
	private String getCampo5() {
		String campo = boleto.getFatorVencimento() + boleto.getValorTitulo();
		return campo;
	}
		
	public String getCodigoBarras() {        
   		String campo = 	getNumero() + String.valueOf(boleto.getMoeda()) + getCampo4() +
        				boleto.getFatorVencimento() + boleto.getValorTitulo() + getCampoLivre();		

		return campo;
	}

	public String getLinhaDigitavel() {
		return 	getCampo1().substring(0,5) + "." + getCampo1().substring(5) + "  " + 
				getCampo2().substring(0,5) + "." + getCampo2().substring(5) + "  " +
				getCampo3().substring(0,5) + "." + getCampo3().substring(5) + "  " +
				getCampo4() + "  " + getCampo5(); 
	}

	public String getCarteira() {
		return boleto.getDvCarteira();
	}
	
}