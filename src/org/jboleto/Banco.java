/*
 * Esta biblioteca e um software livre, que pode ser redistribuido e/ou 
 * modificado sob os termos da Licença Publica Geral Reduzida GNU, 
 * conforme publicada pela Free Software Foundation, versao 2.1 da licenca.
 *
 * Esta biblioteca e distribuida na experanca de ser util aos seus usuarios, 
 * porem NAO TEM NENHUMA GARANTIA, EXPLICITAS OU IMPLICITAS, COMERCIAIS OU 
 * DE ATENDIMENTO A UMA DETERMINADA FINALIDADE. 
 * Veja a Licenca Publica Geral Reduzida GNU para maiores detalhes. 
 * A licenca se encontra no arquivo lgpl-br.txt 
 */

package org.jboleto;

/**
 * @author Flavio Brasil
 *
 */
public interface Banco {

    /**
     * Recupera o codigo do banco
     */
	public String getNumero();
	
    /**
     * Recupera o dv do codigo do banco
     */
	public String getDvNumero();
	
    /**
     * Recupera o dv Carteira
     */
	public String getCarteira();
	
    /**
     * Recupera o numero necessario para a geracao do codigo de barras
     */
	public String getCodigoBarras();
	
    /**
     * Recupera a numeracao para a geracao da linha digitavel do boleto
     */
	public String getLinhaDigitavel();
	
    /**
     * Recupera o d�gito verificador do nosso n�mero
     */
	public String getDvNossoNumero(String nossoNumero);
}
