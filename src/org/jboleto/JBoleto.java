/*
 * Esta biblioteca e um software livre, que pode ser redistribuido e/ou 
 * modificado sob os termos da Licença Publica Geral Reduzida GNU, 
 * conforme publicada pela Free Software Foundation, versao 2.1 da licenca.
 *
 * Esta biblioteca e distribuida na experanca de ser util aos seus usuarios, 
 * porem NAO TEM NENHUMA GARANTIA, EXPLICITAS OU IMPLICITAS, COMERCIAIS OU 
 * DE ATENDIMENTO A UMA DETERMINADA FINALIDADE. 
 * Veja a Licenca Publica Geral Reduzida GNU para maiores detalhes. 
 * A licenca se encontra no arquivo lgpl-br.txt 
 */

package org.jboleto; 

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import org.jboleto.bancos.BancoBrasil;
import org.jboleto.bancos.BancoReal;
import org.jboleto.bancos.Bradesco;
import org.jboleto.bancos.CaixaEconomica;
import org.jboleto.bancos.Hsbc;
import org.jboleto.bancos.Itau;
import org.jboleto.bancos.Santander_033;
import org.jboleto.bancos.Santander_353;
import org.jboleto.bancos.Unibanco;
import org.jboleto.control.PDFGenerator;


/**
 * Classe resposavel por setar todas as configuracoes necessarias para a geracao do titulo / boleto
 * @author Flavio Brasil
 * 
 */
public class JBoleto {
        
	public static int BANCO_DO_BRASIL = 0;
	public static int BRADESCO = 1;
	public static int ITAU = 2;
	public static int BANCO_REAL = 3;
    public static int CAIXA_ECONOMICA = 4;
    public static int UNIBANCO = 5;
    public static int HSBC = 6;
    public static int SANTANDER_033 = 7;
    public static int SANTANDER_353 = 8;
	
    private PDFGenerator generator;

    /**
     * Metodo responsavel por adicionar um boleto na fila para a geracao e identificando o seu respectivo banco
     */    
    public void addBoleto(JBoletoBean boleto, int banco){
        
    	Banco bancoBean = null;
        
    	if(banco==JBoleto.BANCO_DO_BRASIL){
    		bancoBean = new BancoBrasil(boleto);
		}
		else if(banco==JBoleto.BRADESCO){
			bancoBean = new Bradesco(boleto);
		}
		else if(banco==JBoleto.ITAU){
			bancoBean = new Itau(boleto);
		}
		else if(banco==JBoleto.BANCO_REAL){
			bancoBean = new BancoReal(boleto);
		}
		else if(banco==JBoleto.CAIXA_ECONOMICA){
			bancoBean = new CaixaEconomica(boleto);
		}
		else if(banco==JBoleto.UNIBANCO){
			bancoBean = new Unibanco(boleto);
		}
		else if(banco==JBoleto.HSBC){
			bancoBean = new Hsbc(boleto);
		}
		else if(banco==JBoleto.SANTANDER_033){
			bancoBean = new Santander_033(boleto);
		}
		else if(banco==JBoleto.SANTANDER_353){
			bancoBean = new Santander_353(boleto);
		}
        
        if(generator == null) generator = new PDFGenerator();
        	generator.addBoleto(boleto, bancoBean);
    }

    /**
     * Metodo que cria o arquivo loca no disco
     */    
    public void writeToFile(String path){
        ByteArrayOutputStream baos = generator.getBaos();
        try{        
            FileOutputStream fos = new FileOutputStream(path);
            fos.write(baos.toByteArray());
            fos.flush();
            fos.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    /**
     * Metodo que armazena os dados de cada boleto em buffer
     */    
    public ByteArrayOutputStream writeToByteArray(){
        ByteArrayOutputStream baos = generator.getBaos();
        generator = null;
        return baos;
    }
    
    public static int getQtdDigitos(int banco){
    	
    	int qtdDigitos = -1;
    	
    	if (banco == BANCO_DO_BRASIL){
    		qtdDigitos = 17;
    	}
    	
    	else

    	if (banco == BRADESCO){
    		qtdDigitos = 11;
        }
    	
    	else

       	if (banco == ITAU){
       		qtdDigitos = 8;
        }
    	
        else

        if (banco == BANCO_REAL){
        	qtdDigitos = 13;
        }
    	
        else

        if (banco == CAIXA_ECONOMICA){
        	qtdDigitos = 8;
        }
    	
        else

        if (banco == UNIBANCO){
        	qtdDigitos = 14;
        }
    	
        else

        if (banco == HSBC){
        	qtdDigitos = 10;
        }
    	
        if (banco == SANTANDER_033){
        	qtdDigitos = 7;
        }
        
        if (banco == SANTANDER_353){
        	qtdDigitos = 12;
        }
    	
    	return qtdDigitos;
    }
}
